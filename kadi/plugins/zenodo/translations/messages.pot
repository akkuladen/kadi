# Translations template for PROJECT.
# Copyright (C) 2023 ORGANIZATION
# This file is distributed under the same license as the PROJECT project.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2023.
#
msgid ""
msgstr ""
"Project-Id-Version: PROJECT VERSION\n"
"Report-Msgid-Bugs-To: EMAIL@ADDRESS\n"
"POT-Creation-Date: 2023-07-07 09:14+0200\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 2.12.1\n"

#, python-format
msgid "All data will be uploaded as a single ZIP archive corresponding to the exported %(a_open)sRO-Crate data%(a_close)s of this resource, which can be customized below. Note that uploads to Zenodo will not be published directly, i.e. they can be reviewed and adjusted via Zenodo before actually publishing them."
msgstr ""

msgid "Customize export data"
msgstr ""

msgid "Error uploading resource."
msgstr ""

msgid "Invalid form data."
msgstr ""

msgid "Please verify your email address first."
msgstr ""

msgid "Resource uploaded successfully."
msgstr ""

msgid "The description of this resource, which will be converted to HTML."
msgstr ""

msgid "The following Zenodo metadata will be populated automatically:"
msgstr ""

msgid "The following error occured while uploading the resource to Zenodo:"
msgstr ""

#, python-format
msgid "The license of this resource, if applicable and supported by Zenodo, %(em_open)sCC BY 4.0%(em_close)s otherwise."
msgstr ""

msgid "The tags of this resource."
msgstr ""

msgid "The title of this resource."
msgstr ""

msgid "To review and publish your upload via Zenodo, head to the following URL:"
msgstr ""

msgid "Unknown error."
msgstr ""

msgid "Upload canceled."
msgstr ""

msgid "Your current display name und ORCID iD, if available."
msgstr ""

msgid "Zenodo is a general-purpose open-access repository developed and operated by CERN. It allows researchers to deposit and publish data sets, research software, reports, and any other research related digital objects. Connecting your account to Zenodo makes it possible to directly upload resources to Zenodo."
msgstr ""
