Documentation
=============

Documenting source code
-----------------------

Generally, all code should be documented using docstrings in a format suitable for
extraction. Docstrings in JavaScript should follow the `JSDoc <https://jsdoc.app>`__
format. For general conventions about docstrings in Python, take a look at `PEP 257
<https://www.python.org/dev/peps/pep-0257>`__. As for the docstring format, RST
(reStrucuredText) style should be used, like in the following example:

.. code-block:: python3

    def foo(param_a, param_b=None):
        """Brief description, ideally in one line, but can also consist of multiple
        lines, if necessary.

        Longer description and additional information, if necessary. Here we could also
        include an example or anything else that RST allows us to do:

        .. code-block:: python3

            # An example on how to call the function.
            foo(1, param_b=2)

        :param param_a: Description of param_a.
        :param param_b: (optional) Very long description of an optional param_b that may
            take up multiple lines.
        :return: Description of return value.
        :raises Exception: If something went wrong.
        """

Building the documentation
--------------------------

The source files of the documentation can be found in ``docs/source``. Generating HTML
or other output from those files requires `Sphinx
<https://www.sphinx-doc.org/en/master/index.html>`__, which should be installed already,
as well as `make <https://www.gnu.org/software/make>`__. If make is missing, it can be
installed using the following command:

.. code-block:: bash

    sudo apt install make

Afterwards, the documentation can be built in HTML format using:

.. code-block:: bash

    make -C docs/ html

The generated documentation can then be found inside the ``build/html`` directory:

.. code-block:: bash

    firefox docs/build/html/index.html
