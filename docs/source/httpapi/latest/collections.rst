Collections
===========

.. include:: ../snippets/collections.rst

GET
---

.. autoflask:: kadi.wsgi:app
    :packages: kadi.modules.collections.api
    :methods: get
    :autoquickref:

POST
----

.. autoflask:: kadi.wsgi:app
    :packages: kadi.modules.collections.api
    :methods: post
    :autoquickref:

PATCH
-----

.. autoflask:: kadi.wsgi:app
    :packages: kadi.modules.collections.api
    :methods: patch
    :autoquickref:

DELETE
------

.. autoflask:: kadi.wsgi:app
    :packages: kadi.modules.collections.api
    :methods: delete
    :autoquickref:
