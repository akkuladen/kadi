Templates
=========

.. include:: ../snippets/templates.rst

GET
---

.. autoflask:: kadi.wsgi:app
    :packages: kadi.modules.templates.api
    :methods: get
    :autoquickref:

POST
----

.. autoflask:: kadi.wsgi:app
    :packages: kadi.modules.templates.api
    :methods: post
    :autoquickref:

PATCH
-----

.. autoflask:: kadi.wsgi:app
    :packages: kadi.modules.templates.api
    :methods: patch
    :autoquickref:

DELETE
------

.. autoflask:: kadi.wsgi:app
    :packages: kadi.modules.templates.api
    :methods: delete
    :autoquickref:
