.. |sysadmin-ui| replace:: *Can also be set via the graphical sysadmin interface.*

.. _installation-configuration-settings:

Settings
========

This section explains all configuration not related to authentication or storage. This
includes the configuration of all dependencies that |kadi| relies on, as well as other
system-relevant and miscellaneous configuration.

System
------

.. confval:: SERVER_NAME

    The name or IP of the host that |kadi| runs on, e.g. ``"kadi4mat.example.edu"``.

    Defaults to ``None``.

    .. note::
        Note that changing this value **after** performing a production setup requires
        some changes to the generated Apache configuration file as well. Namely, the
        corresponding ``ServerName`` and ``Redirect`` directives need to be adapted
        according to the new server name. Additionally, SSL/TLS certificates might need
        to be updated, e.g. when using a self-signed certificate, a new one needs to be
        generated and configured via the ``SSLCertificate*`` directives. For generating
        such a certificate, or a completely new Apache configuration file, please refer
        back to the :ref:`manual installation <installation-production-manual-apache>`
        instructions.

.. confval:: SECRET_KEY

    The secret key is used for most things that require encryption or signatures in
    |kadi|, so it should be an appropriately secure value and must always be kept
    secret. The following command can be used to generate a suitable value:

    .. code-block:: bash

        tr -dc '[:alnum:]' < /dev/urandom | head -c20

    Defaults to ``None``.

    .. warning::
        Note that changing the secret key will invalidate all encrypted values
        previously stored in the database as well as all signed cookies.

.. confval:: MISC_UPLOADS_PATH

    An absolute local file path in which all miscellaneous uploads are stored in, such
    as profile or group images, e.g. ``"/opt/kadi/uploads"``.

    Note that the corresponding directory structure will be created automatically by the
    application. If you want to create it beforehand, make sure the permissions of at
    least the uploads directory are set up according to the user and group under which
    the application is running, e.g.:

    .. code-block:: bash

        sudo chown kadi:www-data /opt/kadi/uploads
        sudo chmod 750 /opt/kadi/uploads

    Defaults to ``None``.

    .. note::
        Note that changing this value **after** performing a production setup requires
        some changes to the generated Apache configuration file as well. Namely, the
        corresponding ``XSendFilePath`` directive needs to be adapted according to the
        new path. Please also refer back to the :ref:`manual installation
        <installation-production-manual-apache>` instructions.

.. confval:: PROXY_FIX_HEADERS

    This configuration option needs to be used if |kadi| runs behind one or more proxy
    servers in order to ensure that e.g. IP addresses reported to the application cannot
    be easily spoofed by users via HTTP headers. This is done by specifying a dictionary
    in the following form:

    .. code-block:: python3

        {
            "x_for": 1,
            "x_proto": 1,
            "x_host": 0,
            "x_port": 0,
            "x_prefix": 0,
        }

    These values specify which HTTP headers are set by how many proxy servers. The
    headers that are taken into account are:

    * ``X-Forwarded-For``, as specified via ``"x_for"``
    * ``X-Forwarded-Proto``, as specified via ``"x_proto"``
    * ``X-Forwarded-Host``, as specified via ``"x_host"``
    * ``X-Forwarded-Port``, as specified via ``"x_port"``
    * ``X-Forwarded-Prefix``, as specified via ``"x_prefix"``

    Note that when following along with the installation instructions in this
    documentation, the default value should **not** be changed.

    Defaults to ``None``.

.. confval:: EXPERIMENTAL_FEATURES

    Enable experimental features, **which may not yet be stable, fully tested and/or
    feature complete.**

    Defaults to ``False``.

.. confval:: WORKFLOW_FEATURES

    Enable all workflow features that are stable enough to not be classified as
    experimental anymore. Note that these features might still not be complete in
    various ways, but enabling and using them should not lead to any issues.

    Defaults to ``False``. If :confval:`EXPERIMENTAL_FEATURES` is ``True``, this flag
    will automatically be set to ``True`` as well.

    .. note::
        This is a temporary flag that will be removed again once all workflow-related
        functionality is fully usable via |kadi|'s web interface.

Dependencies
------------

.. confval:: SQLALCHEMY_DATABASE_URI

    The database connection string to use for PostgreSQL in the form of
    ``postgresql://<user>:<password>@<host>/<database>``. The database user (``<user>``)
    and database name (``<database>``) will generally be ``kadi``, while the password
    (``<password>``) of the database user is chosen when creating the database user. In
    single-machine setups, the host (``<host>``) is usually just ``localhost``. If the
    used port differs from the default value (``5432``), it can be specified after the
    host as ``<host>:<port>``.

    Defaults to ``None``.

.. confval:: ELASTICSEARCH_HOSTS; ELASTICSEARCH_CONFIG

    The connection configuration to use for Elasticsearch.
    :confval:`ELASTICSEARCH_HOSTS` can either be a single value or a list of values
    specifying one or more Elasticsearch nodes to connect to.
    :confval:`ELASTICSEARCH_CONFIG` can be used to further customize the connection
    parameters in form of a dictionary, whose contents will be passed to the client
    instance of `elasticsearch-py <https://elasticsearch-py.readthedocs.io>`__.

    In particular, these values will need to be changed when running Elasticsearch on a
    different machine/port and using SSL/TLS as well as some form of authentication,
    especially when using a more intricate setup than a simple, single-node deployment.

    Defaults to ``"http://localhost:9200"`` and ``{"timeout": 15}``.

.. confval:: CELERY_BROKER_URL

    The connection string to use for the Celery broker that communicates between |kadi|
    and the Celery workers. Using Redis is recommended, which can be specified in the
    form of ``redis://<user>:<password>@<host>:<port>/<db>``.

    This value only needs to be changed when running Redis on a different machine/port
    and using authentication or when using another Redis database. Note that it should
    be no problem to use the same Redis connection as for
    :confval:`RATELIMIT_STORAGE_URI`.

    Defaults to ``"redis://localhost:6379/0"``.

Emails
------

.. confval:: SMTP_HOST; SMTP_PORT; SMTP_USERNAME; SMTP_PASSWORD; SMTP_USE_TLS; SMTP_TIMEOUT

    These values specify the SMTP connection needed to send emails via |kadi|, most
    importantly the SMTP host (:confval:`SMTP_HOST`) and port (:confval:`SMTP_PORT`). If
    the SMTP server requires authentication, the username and password can be specified
    using :confval:`SMTP_USERNAME` and :confval:`SMTP_PASSWORD`. If
    :confval:`SMTP_USE_TLS` is enabled, STARTTLS will be used for an encrypted SMTP
    connection. Finally, :confval:`SMTP_TIMEOUT` specifies the timeout in seconds when
    connecting to the SMTP server.

    Defaults to ``"localhost"`` (host), ``25`` (port), ``""`` (username), ``""``
    (password), ``True`` (encryption) and ``60`` (timeout).

    .. tip::
        :ref:`Sysadmins <installation-configuration-sysadmins>` can also test the
        sending of emails via the graphical sysadmin interface.

.. confval:: MAIL_NO_REPLY

    The email address that will be used to send no-reply emails from, e.g. for
    confirming email addresses or password reset requests for local accounts.

    Defaults to ``"no-reply@<fqdn>"``, where ``<fqdn>`` is the fully qualified domain
    name of the server.

.. confval:: MAIL_SUBJECT_HEADER

    A header value to be included in the subject field of all emails sent by the
    application. The header and subject values are combined in the form of
    ``[<MAIL_SUBJECT_HEADER>] <subject>``.

    Defaults to ``"Kadi4Mat"``.

.. confval:: MAIL_ERROR_LOGS

    A list of email addresses that will receive logs of unexpected/uncaught errors and
    exceptions using Python's ``SMTPHandler``. As this will send an email for each
    individual error, setting up Sentry via :confval:`SENTRY_DSN` may be the preferred
    way for error monitoring.

    Defaults to ``[]``.

Rate limiting
-------------

.. confval:: RATELIMIT_STORAGE_URI

    The connection string to use for caching information used for rate limiting. Using
    Redis is recommended, which can be specified in the form of
    ``redis://<user>:<password>@<host>:<port>/<db>``.

    This value only needs to be changed when running Redis on a different machine/port
    and using authentication or when using another Redis database. Note that it should
    be no problem to use the same Redis connection as for :confval:`CELERY_BROKER_URL`.

    Defaults to ``"redis://localhost:6379/0"``.

.. confval:: RATELIMIT_ANONYMOUS_USER

    The global default rate limit for anonymous users. Must be a limit string according
    to the specification of the `Flask-Limiter <https://flask-limiter.readthedocs.io>`__
    library.

    Defaults to ``"100/minute;5/second"``.

.. confval:: RATELIMIT_AUTHENTICATED_USER

    The global default rate limit for authenticated users. Must be a limit string
    according to the specification of the `Flask-Limiter
    <https://flask-limiter.readthedocs.io>`__ library.

    Defaults to ``"500/minute;25/second"``.

.. confval:: RATELIMIT_IP_WHITELIST

    A list of IP addresses that are exempt from rate limiting.

    Defaults to ``["127.0.0.1"]``.

Customization
-------------

.. confval:: BROADCAST_MESSAGE

    Markdown formatted text shown at the top of all pages to all authenticated users.
    Users are able to dismiss a broadcast message, however, it will reappear whenever
    its content is changed.

    Defaults to ``""``.

    |sysadmin-ui|

.. confval:: BROADCAST_MESSAGE_PUBLIC

    Whether to show the broadcast message to unauthenticated users as well.

    Defaults to ``False``.

    |sysadmin-ui|

.. confval:: NAV_FOOTER_ITEMS

    A list of additional navigation items, which are shown on all pages in the footer
    next to the existing items. Each item consists of a list containing its title and an
    absolute or relative URL, e.g.:

    .. code-block:: python3

        NAV_FOOTER_ITEMS = [
           ["My Link", "https://example.com"],
        ]

    For more complex customization needs, see also how to :ref:`develop
    <development-plugins>` plugins and the
    :func:`kadi.plugins.spec.kadi_get_nav_footer_items` plugin hook.

    Defaults to ``[]``.

    |sysadmin-ui|

.. confval:: INDEX_IMAGE

    An absolute path to a JPEG or PNG image file, which will be shown on the index page
    next to the :confval:`INDEX_TEXT`. For more complex customization needs, see also
    how to :ref:`develop <development-plugins>` plugins and the
    :func:`kadi.plugins.spec.kadi_get_index_templates` plugin hook.

    Defaults to ``None``.

    |sysadmin-ui|

.. confval:: INDEX_TEXT

    Markdown formatted text, which will be shown on the index page next to the
    :confval:`INDEX_IMAGE`. For more complex customization needs, see also how to
    :ref:`develop <development-plugins>` plugins and the
    :func:`kadi.plugins.spec.kadi_get_index_templates` plugin hook.

    Defaults to ``""``.

    |sysadmin-ui|

Legals
------

.. confval:: TERMS_OF_USE

    Markdown formatted text to specify terms of use. Once set, the terms of use are
    available on a separate page (``/terms-of-use``). A corresponding shortcut to this
    page is automatically created in the navigation footer.

    Defaults to ``""``.

    |sysadmin-ui|

.. confval:: PRIVACY_POLICY

    Markdown formatted text to specify a privacy policy. Once set, the privacy policy is
    available on a separate page (``/privacy-policy``). A corresponding shortcut to this
    page is automatically created in the navigation footer.

    Defaults to ``""``.

    |sysadmin-ui|

.. confval:: ENFORCE_LEGALS

    Require all users to accept the terms of use and/or privacy policy, if at least one
    of them is configured and whenever one of them is changed. If registration of new
    local users is enabled, a corresponding checkbox will be shown in the registration
    form to accept the configured legal notices. Users which had their account created
    via different means, as well as existing users, will be prompted to accept the
    configured or updated legal notices on their next |kadi| interaction instead.

    Defaults to ``False``.

    |sysadmin-ui|

    .. note::
        Note that due to technical limitations, changes to one of the enforced legal
        notices are only tracked when specifying them via the graphical sysadmin
        interface, i.e. only in this case existing users are required to accept them
        again.

.. confval:: LEGAL_NOTICE

    Markdown formatted text to specify a legal notice. Once set, the legal notice is
    available on a separate page (``/legal-notice``). A corresponding shortcut to this
    page is automatically created in the navigation footer.

    Defaults to ``""``.

    |sysadmin-ui|

Miscellaneous
-------------

.. confval:: ROBOTS_NOINDEX

    Exclude a public |kadi| instance from being indexed by search engines. This applies
    to the entire website, including all outgoing links. When set retroactively, it
    might take a while until existing results are deleted from the various search
    indices.

    Note that some web crawlers might not recognize this option, but it should work for
    all major search engines.

    Defaults to ``False``.

    |sysadmin-ui|

.. confval:: SENTRY_DSN

    A Sentry DSN (Data Source Name) which can be used to integrate the `Sentry
    <https://sentry.io>`__ error monitoring tool with |kadi|.

    Defaults to ``None``.

Plugins
-------

.. confval:: PLUGINS; PLUGIN_CONFIG

    These values are used to enable and configure plugins. See the :ref:`Plugins
    <installation-plugins>` section for more information.

    Defaults to ``[]`` and ``{}``.
