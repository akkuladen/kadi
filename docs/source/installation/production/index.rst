.. _installation-production:

Production
==========

.. note::
    The productive installation has currently been tested under the following
    Debian-based Linux distributions, which all instructions and provided scripts are
    based on:

    * **Debian 11** (Bullseye)
    * **Debian 12** (Bookworm)
    * **Ubuntu 22.04 LTS** (Jammy Jellyfish)
    * **Ubuntu 24.04 LTS** (Noble Numbat)

This section describes how to install, :ref:`update <installation-production-updating>`
and :ref:`backup <installation-production-backup>` |kadi| in a production environment
using a simple, single-machine setup. This type of setup should already be sufficient
for most |kadi| instances.

The required hardware for this setup may vary for each use case, especially in regards
to the required storage for the database, the search index and locally stored files. As
a general rule of thumb for RAM and CPU, a minimum of **16 GB RAM** and a decent **CPU
with 8 cores** should be sufficient for this type of setup.

.. warning::
    Keep in mind that there are various :ref:`security considerations
    <installation-production-security>` to take into account when deploying |kadi| in
    production.

There are currently two ways to perform the installation: installation via :ref:`setup
script <installation-production-script>` and :ref:`manual
<installation-production-manual>` installation. When using the former, it is still
recommended to check out the manual installation instructions, as all the necessary
steps are described in much more detail. This is also recommended when planning to
deploy a more intricate, multi-machine setup.

.. note::
    After the installation, please make sure to at least check out the
    :ref:`configuration <installation-configuration>` reference as well, which explains
    how to further configure and customize |kadi|, especially with regard to user
    authentication.

.. toctree::
    :maxdepth: 1

    script
    manual
    updating
    backup
    python
    security
