# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from flask import current_app
from flask import get_flashed_messages
from flask_login import current_user

import kadi.lib.constants as const
from kadi.lib.config.core import set_sys_config
from kadi.lib.web import url_for
from kadi.modules.accounts.models import UserState
from tests.utils import check_api_response
from tests.utils import check_view_response


def test_before_app_request_session(monkeypatch, client, dummy_user, user_session):
    """Test the "before_app_request" handler using a session."""
    endpoint = url_for("main.index")

    # Check if an invalid identity is handled correctly.
    with user_session():
        with monkeypatch.context() as m:
            m.setitem(current_app.config, "AUTH_PROVIDERS", [])

            response = client.get(endpoint)

            check_view_response(response, status_code=302)
            assert response.location == url_for("main.index")
            assert "This account is currently inactive." in get_flashed_messages()
            assert not current_user.is_authenticated

    # Check if an unconfirmed email is handled correctly.
    with monkeypatch.context() as m:
        m.setitem(
            current_app.config["AUTH_PROVIDERS"][const.AUTH_PROVIDER_TYPE_LOCAL],
            "email_confirmation_required",
            True,
        )

        with user_session():
            response = client.get(endpoint)

            check_view_response(response, status_code=302)
            assert response.location == url_for("accounts.request_email_confirmation")

    # Check if an inactive user is handled correctly.
    with user_session():
        with monkeypatch.context() as m:
            m.setattr(dummy_user, "state", UserState.INACTIVE)

            response = client.get(endpoint)

            check_view_response(response, status_code=302)
            assert response.location == url_for("accounts.inactive_user")

    # Check if enforced legal notices that were not accepted are handled correctly.
    set_sys_config(const.SYS_CONFIG_TERMS_OF_USE, "Test")
    set_sys_config(const.SYS_CONFIG_ENFORCE_LEGALS, True)

    with user_session():
        response = client.get(endpoint)

        check_view_response(response, status_code=302)
        assert response.location == url_for("accounts.request_legals_acceptance")


def test_before_app_request_api(
    monkeypatch, api_client, dummy_personal_token, dummy_user
):
    """Test the "before_app_request" handler using an access token."""
    endpoint = url_for("api.index")

    # Check if an invalid identity is handled correctly.
    with monkeypatch.context() as m:
        m.setitem(current_app.config, "AUTH_PROVIDERS", [])

        response = api_client(dummy_personal_token).get(endpoint)

        check_api_response(response, status_code=401)
        assert (
            "This account is currently inactive." in response.get_json()["description"]
        )

    # Check if an unconfirmed email is handled correctly.
    with monkeypatch.context() as m:
        m.setitem(
            current_app.config["AUTH_PROVIDERS"][const.AUTH_PROVIDER_TYPE_LOCAL],
            "email_confirmation_required",
            True,
        )

        response = api_client(dummy_personal_token).get(endpoint)

        check_api_response(response, status_code=401)
        assert (
            "Please confirm your email address." in response.get_json()["description"]
        )

    # Check if an inactive user is handled correctly.
    with monkeypatch.context() as m:
        m.setattr(dummy_user, "state", UserState.INACTIVE)

        response = api_client(dummy_personal_token).get(endpoint)

        check_api_response(response, status_code=401)
        assert (
            "This account is currently inactive." in response.get_json()["description"]
        )

    # Check if enforced legal notices that were not accepted are handled correctly.
    set_sys_config(const.SYS_CONFIG_TERMS_OF_USE, "Test")
    set_sys_config(const.SYS_CONFIG_ENFORCE_LEGALS, True)

    response = api_client(dummy_personal_token).get(endpoint)

    check_api_response(response, status_code=401)
    assert "Please accept all legal notices." in response.get_json()["description"]
