# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import math

import pytest
from flask import json
from marshmallow import ValidationError
from werkzeug.datastructures import MultiDict

import kadi.lib.constants as const
from kadi.lib.conversion import parse_datetime_string
from kadi.lib.forms import KadiForm
from kadi.modules.records.extras import ExtraSchema
from kadi.modules.records.extras import ExtrasField
from kadi.modules.records.extras import remove_extra_values
from tests.constants import DUMMY_EXTRA_NO_VALUE
from tests.constants import DUMMY_EXTRA_VALUE


@pytest.mark.parametrize(
    "data,result",
    [
        ([DUMMY_EXTRA_VALUE], [DUMMY_EXTRA_NO_VALUE]),
        (
            [{"type": "dict", "key": "test", "value": [DUMMY_EXTRA_VALUE]}],
            [{"type": "dict", "key": "test", "value": [DUMMY_EXTRA_NO_VALUE]}],
        ),
    ],
)
def test_remove_extra_values(data, result):
    """Test if values of extras are removed correctly."""
    assert remove_extra_values(data) == result


def test_extras_jsonb(dummy_record):
    """Test if the custom JSONB type for extras works correctly."""
    dummy_record.extras = [{"type": "float", "key": "test", "value": 1e100}]
    assert isinstance(dummy_record.extras[0]["value"], float)


@pytest.mark.parametrize(
    "data,extra,is_template",
    [
        # Trimming of type, normalization of key and default value.
        (
            {"type": " str ", "key": " te  st "},
            {"type": "str", "key": "te st", "value": None},
            False,
        ),
        # Trimming of a string value.
        (
            {"type": "str", "key": "test", "value": " test "},
            {"type": "str", "key": "test", "value": "test"},
            False,
        ),
        # Replacement of empty strings and unit with an invalid type.
        (
            {"type": "str", "key": "test", "value": " ", "unit": None},
            {"type": "str", "key": "test", "value": None},
            False,
        ),
        # Default unit value.
        (
            {"type": "int", "key": "test"},
            {"type": "int", "key": "test", "value": None, "unit": None},
            False,
        ),
        # Normalization of unit value.
        (
            {"type": "int", "key": "test", "unit": " foo  bar "},
            {"type": "int", "key": "test", "value": None, "unit": "foo bar"},
            False,
        ),
        # Int value.
        (
            {"type": "int", "key": "test", "value": 123},
            {"type": "int", "key": "test", "value": 123, "unit": None},
            False,
        ),
        # Float value as an int.
        (
            {"type": "float", "key": "test", "value": -123, "unit": None},
            None,
            False,
        ),
        # Float value in scientific notation.
        (
            {
                "type": "float",
                "key": "test",
                "value": 1e123,
            },
            {"type": "float", "key": "test", "value": 1e123, "unit": None},
            False,
        ),
        # Date value in "Zulu" time.
        (
            {"type": "date", "key": "test", "value": "2020-01-01T12:34:56.789Z"},
            {
                "type": "date",
                "key": "test",
                "value": "2020-01-01T12:34:56.789000+00:00",
            },
            False,
        ),
        # Date value as "datetime" object.
        (
            {
                "type": "date",
                "key": "test",
                "value": parse_datetime_string("2020-01-01T12:34:56.789Z"),
            },
            {
                "type": "date",
                "key": "test",
                "value": "2020-01-01T12:34:56.789000+00:00",
            },
            False,
        ),
        # Boolean value.
        (
            {"type": "bool", "key": "test", "value": True},
            None,
            False,
        ),
        # Empty dictionary value.
        (
            {"type": "dict", "key": "test"},
            {"type": "dict", "key": "test", "value": []},
            False,
        ),
        # Empty list value.
        (
            {"type": "list", "key": "test", "value": []},
            None,
            False,
        ),
        # Dict value with a nested string.
        (
            {
                "type": "dict",
                "key": "test",
                "value": [{"type": "str", "key": "test", "value": "test"}],
            },
            None,
            False,
        ),
        # List value with a nested string.
        (
            {
                "type": "list",
                "key": "test",
                "value": [{"type": "str", "value": "test"}],
            },
            None,
            False,
        ),
        # Description value.
        (
            {"type": "str", "key": "test", "value": "test", "description": "test"},
            None,
            False,
        ),
        # Term value.
        (
            {"type": "str", "key": "test", "value": "test", "term": "test"},
            None,
            False,
        ),
        # Empty validation instructions.
        (
            {"type": "str", "key": "test", "value": "test", "validation": {}},
            {"type": "str", "key": "test", "value": "test"},
            False,
        ),
        # Required validation that can be omitted.
        (
            {
                "type": "str",
                "key": "test",
                "value": None,
                "validation": {"required": False},
            },
            {"type": "str", "key": "test", "value": None},
            False,
        ),
        # Required validation.
        (
            {
                "type": "str",
                "key": "test",
                "value": "test",
                "validation": {"required": True},
            },
            None,
            False,
        ),
        # Required validation for an empty value in a template.
        (
            {
                "type": "str",
                "key": "test",
                "value": None,
                "validation": {"required": True},
            },
            None,
            True,
        ),
        # Required validation for an empty, nested value in a template.
        (
            {
                "type": "dict",
                "key": "test",
                "value": [
                    {
                        "type": "str",
                        "key": "test",
                        "value": None,
                        "validation": {"required": True},
                    }
                ],
            },
            None,
            True,
        ),
        # IRI validation that can be omitted.
        (
            {
                "type": "str",
                "key": "test",
                "value": None,
                "validation": {"iri": False},
            },
            {"type": "str", "key": "test", "value": None},
            False,
        ),
        # IRI validation.
        (
            {
                "type": "str",
                "key": "test",
                "value": "test",
                "validation": {"iri": True},
            },
            None,
            False,
        ),
        # Range validation that can be omitted.
        (
            {
                "type": "int",
                "key": "test",
                "value": None,
                "validation": {"range": {"max": None}},
            },
            {"type": "int", "key": "test", "value": None, "unit": None},
            False,
        ),
        # Range validation without a value and without a max range.
        (
            {"type": "int", "key": "test", "validation": {"range": {"min": 1}}},
            {
                "type": "int",
                "key": "test",
                "value": None,
                "unit": None,
                "validation": {"range": {"min": 1, "max": None}},
            },
            False,
        ),
        # Range validation with a min int value.
        (
            {
                "type": "int",
                "key": "test",
                "value": 1,
                "unit": None,
                "validation": {"range": {"min": 1, "max": None}},
            },
            None,
            False,
        ),
        # Range validation with a min and max (as int) float value.
        (
            {
                "type": "float",
                "key": "test",
                "value": 2.0,
                "unit": None,
                "validation": {"required": True, "range": {"min": 1.0, "max": 3}},
            },
            None,
            False,
        ),
        # Options validation that can be omitted.
        (
            {
                "type": "str",
                "key": "test",
                "value": None,
                "validation": {"options": []},
            },
            {
                "type": "str",
                "key": "test",
                "value": None,
            },
            False,
        ),
        # Options validation without a value.
        (
            {
                "type": "str",
                "key": "test",
                "validation": {"options": ["test"]},
            },
            {
                "type": "str",
                "key": "test",
                "value": None,
                "validation": {"options": ["test"]},
            },
            False,
        ),
        # Options validation with one duplicate option.
        (
            {
                "type": "str",
                "key": "test",
                "value": None,
                "validation": {"options": ["test", "test"]},
            },
            {
                "type": "str",
                "key": "test",
                "value": None,
                "validation": {"options": ["test"]},
            },
            False,
        ),
        # Combination of required and options validation.
        (
            {
                "type": "str",
                "key": "test",
                "value": "test",
                "validation": {"required": True, "options": ["test"]},
            },
            None,
            False,
        ),
        # Combination of iri and options validation.
        (
            {
                "type": "str",
                "key": "test",
                "value": "test",
                "validation": {"iri": True, "options": ["test"]},
            },
            None,
            False,
        ),
        # Combination of range and required validation.
        (
            {
                "type": "float",
                "key": "test",
                "value": 2.0,
                "unit": None,
                "validation": {"range": {"min": 1.1, "max": None}, "required": True},
            },
            None,
            False,
        ),
        # Combination of range and options validation.
        (
            {
                "type": "float",
                "key": "test",
                "value": 1.5,
                "unit": None,
                "validation": {"range": {"min": 1.0, "max": 2.0}, "options": [1.5]},
            },
            None,
            False,
        ),
    ],
)
def test_extra_schema_success(data, extra, is_template):
    """Test if the extra schema validates valid extras correctly."""
    if extra is None:
        extra = data

    assert ExtraSchema(is_template=is_template).load(data) == extra


@pytest.mark.parametrize(
    "data",
    [
        # Invalid input type.
        None,
        # Valid input type, but empty.
        {},
        # Missing a key.
        {"type": "str"},
        # Invalid key.
        {"type": "str", "key": None},
        # Invalid key.
        {"type": "str", "key": " "},
        # Invalid type.
        {"type": None, "key": "test"},
        # Invalid type.
        {"type": "test", "key": "test"},
        # Unit with an invalid type.
        {"type": "str", "key": "test", "unit": "test"},
        # Invalid unit.
        {"type": "int", "key": "test", "unit": " "},
        # Invalid unit.
        {"type": "int", "key": "test", "unit": 123},
        # Invalid string value.
        {"type": "str", "key": "test", "value": 123},
        # Invalid int value.
        {"type": "int", "key": "test", "value": "123"},
        # Invalid int value.
        {"type": "int", "key": "test", "value": 123.0},
        # Invalid int value.
        {"type": "int", "key": "test", "value": const.EXTRAS_MAX_INTEGER + 1},
        # Invalid int value.
        {"type": "int", "key": "test", "value": const.EXTRAS_MIN_INTEGER - 1},
        # Invalid float value.
        {"type": "float", "key": "test", "value": math.inf},
        # Invalid float value.
        {"type": "float", "key": "test", "value": math.nan},
        # Invalid bool value.
        {"type": "bool", "key": "test", "value": "true"},
        # Invalid date value.
        {"type": "date", "key": "test", "value": "test"},
        # Invalid input type for nested value.
        {"type": "dict", "key": "test", "value": {}},
        # Invalid input type for nested value.
        {"type": "dict", "key": "test", "value": [None]},
        # Duplicate key within a nested value.
        {
            "type": "dict",
            "key": "test",
            "value": [
                {"type": "str", "key": "test"},
                {"type": "str", "key": "test"},
            ],
        },
        # Invalid description.
        {"type": "str", "key": "test", "description": None},
        # Invalid term.
        {"type": "str", "key": "test", "term": None},
        # Invalid term.
        {"type": "str", "key": "test", "term": " "},
        # Invalid term.
        {"type": "str", "key": "test", "term": "<"},
        # Invalid input type for validation.
        {"type": "str", "key": "test", "validation": None},
        # Validation for a nested type.
        {"type": "dict", "key": "test", "validation": {"required": True}},
        # Required validation with a missing value.
        {"type": "str", "key": "test", "validation": {"required": True}},
        # Required validation with a missing value in a nested context.
        {
            "type": "dict",
            "key": "test",
            "value": [{"type": "str", "key": "test", "validation": {"required": True}}],
        },
        # IRI validation with an invalid type.
        {"type": "int", "key": "test", "value": "test", "validation": {"iri": True}},
        # IRI validation with an invalid value.
        {"type": "str", "key": "test", "value": "<", "validation": {"iri": True}},
        # Range validation with an invalid type.
        {
            "type": "str",
            "key": "test",
            "value": "test",
            "validation": {"range": {"min": 0}},
        },
        # Range validation with an invalid range.
        {
            "type": "int",
            "key": "test",
            "validation": {"range": {"min": "a", "max": True}},
        },
        # Range validation with an invalid range.
        {
            "type": "int",
            "key": "test",
            "validation": {"range": {"max": const.EXTRAS_MAX_INTEGER + 1}},
        },
        # Range validation with an invalid range.
        {
            "type": "float",
            "key": "test",
            "validation": {"range": {"min": math.nan}},
        },
        # Range validation with an invalid value.
        {
            "type": "int",
            "key": "test",
            "value": 1,
            "validation": {"range": {"min": 2}},
        },
        # Range validation with a min value larger than the max one.
        {"type": "int", "key": "test", "validation": {"range": {"min": 2, "max": 1}}},
        # Options validation with an invalid type.
        {"type": "bool", "key": "test", "validation": {"options": [True]}},
        # Options validation with invalid options.
        {"type": "str", "key": "test", "validation": {"options": [1]}},
        # Options validation with invalid options.
        {"type": "str", "key": "test", "validation": {"iri": True, "options": ["<"]}},
        # Options validation with an invalid value.
        {
            "type": "str",
            "key": "test",
            "value": "test",
            "validation": {"options": ["test2"]},
        },
    ],
)
def test_extra_schema_error(data):
    """Test if the extra schema validates invalid extras correctly."""
    with pytest.raises(ValidationError):
        ExtraSchema().load(data)


@pytest.mark.parametrize(
    "data,is_valid,result,formdata",
    [
        (None, True, [], []),
        (
            [{"type": "str", "key": "test"}],
            True,
            [{"type": "str", "key": "test", "value": None}],
            [
                {
                    "type": {"value": "str", "errors": []},
                    "key": {"value": "test", "errors": []},
                    "value": {"value": None, "errors": []},
                    "unit": {"value": None, "errors": []},
                    "description": {"value": None, "errors": []},
                    "term": {"value": None, "errors": []},
                    "validation": {"value": None, "errors": []},
                }
            ],
        ),
        (
            [{"type": "str", "key": "test", "validation": {"required": True}}],
            False,
            [],
            [
                {
                    "type": {"value": "str", "errors": []},
                    "key": {"value": "test", "errors": []},
                    "value": {"value": None, "errors": ["Value is required."]},
                    "unit": {"value": None, "errors": []},
                    "description": {"value": None, "errors": []},
                    "term": {"value": None, "errors": []},
                    "validation": {"value": {"required": True}, "errors": []},
                }
            ],
        ),
    ],
)
def test_extras_field(data, is_valid, result, formdata):
    """Test if the extras form field works correctly."""

    class _TestForm(KadiForm):
        test = ExtrasField(is_template=False)

    if isinstance(data, list):
        data = {"test": json.dumps(data)}
    elif data is not None:
        data = {"test": data}

    form = _TestForm(formdata=MultiDict(data))

    assert form.validate() is is_valid
    assert form.test.data == result
    assert form.test._value() == formdata

    if not is_valid:
        assert "Invalid data structure." in form.errors["test"]
