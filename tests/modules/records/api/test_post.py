# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from io import BytesIO

import pytest
from flask import current_app

import kadi.lib.constants as const
from kadi.lib.web import url_for
from kadi.modules.records.core import delete_record
from kadi.modules.records.models import Record
from kadi.modules.records.models import RecordLink
from kadi.modules.records.models import RecordState
from kadi.modules.records.models import Upload
from kadi.modules.records.models import UploadState
from kadi.modules.records.uploads import save_chunk_data
from tests.modules.utils import check_api_post_subject_resource_role
from tests.utils import check_api_response


def test_new_record(api_client, dummy_personal_token):
    """Test the "api.new_record" endpoint."""
    response = api_client(dummy_personal_token).post(
        url_for("api.new_record"), json={"identifier": "test", "title": "test"}
    )

    check_api_response(response, status_code=201)
    assert Record.query.filter_by(identifier="test").one()


def test_new_record_link(api_client, dummy_personal_token, dummy_record, new_record):
    """Test the "api.new_record_link" endpoint."""
    record = new_record()

    response = api_client(dummy_personal_token).post(
        url_for("api.new_record_link", id=dummy_record.id),
        json={"name": "test", "record_to": {"id": record.id}},
    )

    check_api_response(response, status_code=201)
    assert dummy_record.links_to.filter(RecordLink.name == "test").one()


def test_add_record_collection(
    api_client, dummy_collection, dummy_personal_token, dummy_record
):
    """Test the "api.add_record_collection" endpoint."""
    response = api_client(dummy_personal_token).post(
        url_for("api.add_record_collection", id=dummy_record.id),
        json={"id": dummy_collection.id},
    )

    check_api_response(response, status_code=201)
    assert dummy_record.collections.one() == dummy_collection


def test_add_record_user_role(
    api_client, db, dummy_personal_token, dummy_record, new_user
):
    """Test the "api.add_record_user_role" endpoint."""
    client = api_client(dummy_personal_token)
    endpoint = url_for("api.add_record_user_role", id=dummy_record.id)

    check_api_post_subject_resource_role(db, client, endpoint, new_user(), dummy_record)


def test_add_record_group_role(
    api_client, db, dummy_personal_token, dummy_record, dummy_group
):
    """Test the "api.add_record_group_role" endpoint."""
    client = api_client(dummy_personal_token)
    endpoint = url_for("api.add_record_group_role", id=dummy_record.id)

    check_api_post_subject_resource_role(
        db, client, endpoint, dummy_group, dummy_record
    )


def test_restore_record(api_client, dummy_personal_token, dummy_record, dummy_user):
    """Test the "api.restore_record" endpoint."""
    delete_record(dummy_record, user=dummy_user)

    response = api_client(dummy_personal_token).post(
        url_for("api.restore_record", id=dummy_record.id)
    )

    check_api_response(response)
    assert dummy_record.state == RecordState.ACTIVE


def test_purge_record(api_client, dummy_personal_token, dummy_record, dummy_user):
    """Test the "api.purge_record" endpoint."""
    delete_record(dummy_record, user=dummy_user)

    response = api_client(dummy_personal_token).post(
        url_for("api.purge_record", id=dummy_record.id)
    )

    check_api_response(response, status_code=202)
    assert Record.query.get(dummy_record.id) is None


def test_new_upload(
    monkeypatch, api_client, dummy_personal_token, dummy_file, dummy_record
):
    """Test the "api.new_upload" endpoint."""
    monkeypatch.setitem(current_app.config, "UPLOAD_USER_QUOTA", dummy_file.size)

    endpoint = url_for("api.new_upload", id=dummy_record.id)

    response = api_client(dummy_personal_token).post(
        endpoint, json={"name": "test", "size": 0}
    )

    check_api_response(response, status_code=201)
    assert Upload.query.filter_by(name="test").one()

    response = api_client(dummy_personal_token).post(
        endpoint, json={"name": dummy_file.name, "size": 0}
    )

    check_api_response(response, status_code=409)
    assert Upload.query.filter_by(name=dummy_file.name).first() is None

    response = api_client(dummy_personal_token).post(
        endpoint, json={"name": "test2", "size": 1}
    )

    check_api_response(response, status_code=413)
    assert Upload.query.filter_by(name="test2").one().state == UploadState.INACTIVE


def test_finish_upload(api_client, dummy_personal_token, dummy_record, dummy_upload):
    """Test the "api.finish_upload" endpoint."""
    client = api_client(dummy_personal_token)
    endpoint = url_for(
        "api.finish_upload", record_id=dummy_record.id, upload_id=dummy_upload.id
    )

    response = client.post(endpoint)

    check_api_response(response, status_code=400)

    save_chunk_data(dummy_upload, BytesIO(b""), index=0, size=0)
    response = client.post(endpoint)

    check_api_response(response, status_code=202)


@pytest.mark.parametrize(
    "size,checksum,status_code",
    [(1, None, 201), (0, None, 400), (1, "test", 400), (2, None, 413)],
)
def test_upload_file_legacy(
    size,
    checksum,
    status_code,
    monkeypatch,
    api_client,
    dummy_personal_token,
    dummy_record,
):
    """Test the "api.upload_file_legacy" endpoint."""
    monkeypatch.setattr(
        "kadi.modules.records.api.public.post.const.UPLOAD_CHUNKED_BOUNDARY", 1
    )

    form_data = {
        "name": "test",
        "size": size,
        "blob": (BytesIO(b"x"), "file"),
    }

    if checksum is not None:
        form_data["checksum"] = checksum

    response = api_client(dummy_personal_token).post(
        url_for("api.upload_file_legacy", id=dummy_record.id),
        data=form_data,
        content_type=const.MIMETYPE_FORMDATA,
    )
    check_api_response(response, status_code=status_code)


@pytest.mark.parametrize("replace_file,status_code", [(True, 201), (False, 409)])
def test_upload_file_legacy_replace_file(
    replace_file,
    status_code,
    api_client,
    dummy_file,
    dummy_personal_token,
    dummy_record,
):
    """Test the "api.upload_file_legacy" endpoint when replacing files."""
    form_data = {
        "name": dummy_file.name,
        "size": "1",
        "blob": (BytesIO(b"x"), "file"),
        "replace_file": "true" if replace_file else "false",
    }

    response = api_client(dummy_personal_token).post(
        url_for("api.upload_file_legacy", id=dummy_record.id),
        data=form_data,
        content_type=const.MIMETYPE_FORMDATA,
    )
    check_api_response(response, status_code=status_code)
