# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from io import BytesIO

from flask import current_app

from kadi.lib.security import hash_value
from kadi.lib.web import url_for
from kadi.modules.records.models import File
from kadi.modules.records.models import Upload
from kadi.modules.records.models import UploadState
from kadi.modules.records.models import UploadType
from tests.utils import check_api_response


def test_edit_file_data(
    monkeypatch, api_client, dummy_file, dummy_personal_token, dummy_record
):
    """Test the "api.edit_file_data" endpoint."""
    monkeypatch.setitem(current_app.config, "UPLOAD_USER_QUOTA", dummy_file.size)

    endpoint = url_for(
        "api.edit_file_data", record_id=dummy_record.id, file_id=dummy_file.id
    )

    response = api_client(dummy_personal_token).put(endpoint, json={"size": 0})

    check_api_response(response, status_code=201)
    assert Upload.query.filter_by(name=dummy_file.name).one()

    response = api_client(dummy_personal_token).put(
        endpoint, json={"size": dummy_file.size + 1}
    )

    check_api_response(response, status_code=413)
    assert (
        Upload.query.filter_by(name=dummy_file.name)
        .order_by(Upload.created_at.desc())
        .first()
        .state
        == UploadState.INACTIVE
    )


def test_upload_data_direct(api_client, dummy_personal_token, dummy_record, new_upload):
    """Test the "api.upload_data" endpoint for direct uploads."""
    data = b"x"
    upload = new_upload(size=len(data), upload_type=UploadType.DIRECT)
    checksum = hash_value(data, alg="md5")
    client = api_client(dummy_personal_token)

    response = api_client(dummy_personal_token).put(
        url_for("api.upload_data", record_id=dummy_record.id, upload_id=upload.id),
        data=BytesIO(data),
        headers={
            "Authorization": f"Bearer {client.token}",
            "Kadi-MD5-Checksum": checksum,
        },
    )

    check_api_response(response, status_code=201)
    assert File.query.get(response.get_json()["id"])


def test_upload_data_chunked(
    api_client, dummy_personal_token, dummy_record, new_upload
):
    """Test the "api.upload_data" endpoint for chunked uploads."""
    data = b"x"
    upload = new_upload(size=len(data))
    checksum = hash_value(data, alg="md5")
    client = api_client(dummy_personal_token)

    response = api_client(dummy_personal_token).put(
        url_for("api.upload_data", record_id=dummy_record.id, upload_id=upload.id),
        data=BytesIO(data),
        headers={
            "Authorization": f"Bearer {client.token}",
            "Kadi-MD5-Checksum": checksum,
            "Kadi-Chunk-Index": "0",
            "Kadi-Chunk-Size": str(len(data)),
        },
    )

    check_api_response(response, status_code=200)
    assert upload.active_chunks.one()
