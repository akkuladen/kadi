# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from kadi.lib.web import url_for
from kadi.modules.records.links import create_record_link
from tests.modules.utils import check_api_patch_subject_resource_role
from tests.utils import check_api_response


def test_toggle_favorite_record(client, dummy_record, dummy_user, user_session):
    """Test the internal "api.toggle_favorite_record" endpoint."""
    with user_session():
        response = client.patch(
            url_for("api.toggle_favorite_record", id=dummy_record.id)
        )

        check_api_response(response, status_code=204)
        assert dummy_record.is_favorite(user=dummy_user)


def test_edit_record(api_client, dummy_personal_token, dummy_record):
    """Test the "api.edit_record" endpoint."""
    response = api_client(dummy_personal_token).patch(
        url_for("api.edit_record", id=dummy_record.id), json={"identifier": "test"}
    )

    check_api_response(response)
    assert dummy_record.identifier == "test"


def test_edit_record_link(
    api_client, dummy_personal_token, dummy_record, dummy_user, new_record
):
    """Test the "api.edit_record_link" endpoint."""
    record_link = create_record_link(
        name="test",
        record_from=dummy_record,
        record_to=new_record(),
        creator=dummy_user,
    )

    response = api_client(dummy_personal_token).patch(
        url_for(
            "api.edit_record_link", record_id=dummy_record.id, link_id=record_link.id
        ),
        json={"name": "test2"},
    )

    check_api_response(response)
    assert record_link.name == "test2"


def test_change_record_user_role(
    api_client, db, dummy_personal_token, dummy_record, dummy_user, new_user
):
    """Test the "api.change_record_user_role" endpoint."""
    user = new_user()
    client = api_client(dummy_personal_token)
    endpoint = url_for(
        "api.change_record_user_role", record_id=dummy_record.id, user_id=user.id
    )
    change_creator_endpoint = url_for(
        "api.change_record_user_role", record_id=dummy_record.id, user_id=dummy_user.id
    )

    check_api_patch_subject_resource_role(
        db,
        client,
        endpoint,
        user,
        dummy_record,
        change_creator_endpoint=change_creator_endpoint,
    )


def test_change_record_group_role(
    api_client, db, dummy_group, dummy_personal_token, dummy_record
):
    """Test the "api.change_record_group_role" endpoint."""
    client = api_client(dummy_personal_token)
    endpoint = url_for(
        "api.change_record_group_role",
        record_id=dummy_record.id,
        group_id=dummy_group.id,
    )

    check_api_patch_subject_resource_role(
        db, client, endpoint, dummy_group, dummy_record
    )


def test_edit_file_metadata(api_client, dummy_file, dummy_personal_token, dummy_record):
    """Test the "api.edit_file_metadata" endpoint."""
    new_name = "test"

    response = api_client(dummy_personal_token).patch(
        url_for(
            "api.edit_file_metadata", record_id=dummy_record.id, file_id=dummy_file.id
        ),
        json={"name": new_name},
    )

    check_api_response(response)
    assert dummy_file.name == new_name
