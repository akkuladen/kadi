# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from kadi.lib.permissions.core import add_role
from kadi.lib.resources.utils import add_link
from kadi.lib.tags.models import Tag
from kadi.modules.records.core import create_record
from kadi.modules.records.core import delete_record
from kadi.modules.records.core import purge_record
from kadi.modules.records.core import restore_record
from kadi.modules.records.core import update_record
from kadi.modules.records.links import create_record_link
from kadi.modules.records.models import Record
from kadi.modules.records.models import RecordState
from kadi.modules.records.models import RecordVisibility


def test_create_record(dummy_license, dummy_user):
    """Test if records are created correctly."""
    record = create_record(
        creator=dummy_user,
        identifier="test",
        title="test",
        type="test",
        description="# test",
        license=dummy_license.name,
        extras=[{"type": "str", "key": "sample_key", "value": "sample_value"}],
        visibility=RecordVisibility.PRIVATE,
        tags=["test"],
    )

    assert Record.query.filter_by(identifier="test").one() == record
    assert record.plain_description == "test"
    assert record.license == dummy_license
    assert record.revisions.count() == 1
    assert Tag.query.filter_by(name="test").one()
    assert dummy_user.roles.filter_by(
        name="admin", object="record", object_id=record.id
    ).one()


def test_update_record(dummy_license, dummy_record, dummy_user):
    """Test if records are updated correctly."""
    update_record(
        dummy_record,
        description="# test",
        license=dummy_license.name,
        tags=["test"],
        user=dummy_user,
    )

    assert dummy_record.plain_description == "test"
    assert dummy_record.license == dummy_license
    assert dummy_record.revisions.count() == 2
    assert Tag.query.filter_by(name="test").one()


def test_delete_record(dummy_record, dummy_user):
    """Test if records are deleted correctly."""
    delete_record(dummy_record, user=dummy_user)

    assert dummy_record.state == RecordState.DELETED
    assert dummy_record.revisions.count() == 2


def test_restore_record(dummy_record, dummy_user):
    """Test if records are restored correctly."""
    delete_record(dummy_record, user=dummy_user)
    restore_record(dummy_record, user=dummy_user)

    assert dummy_record.state == RecordState.ACTIVE
    assert dummy_record.revisions.count() == 3


def test_purge_record(
    db,
    dummy_collection,
    dummy_file,
    dummy_record,
    dummy_upload,
    dummy_user,
    new_user,
    new_record,
):
    """Test if records are purged correctly."""
    user = new_user()
    record = new_record()

    add_role(user, "record", record.id, "collaborator")
    add_link(record.collections, dummy_collection, user=dummy_user)
    create_record_link(
        name="test", record_from=record, record_to=dummy_record, creator=dummy_user
    )

    prev_timestamp = dummy_record.last_modified

    purge_record(record)

    assert Record.query.get(record.id) is None
    # Only the system role should remain.
    assert user.roles.count() == 1
    # Check if the linked record was also updated and a new revision was created without
    # an associated user.
    assert dummy_record.last_modified != prev_timestamp
    assert dummy_record.revisions.count() == 3
    assert dummy_record.ordered_revisions.first().revision.user is None
