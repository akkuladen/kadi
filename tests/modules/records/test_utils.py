# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from io import BytesIO

from flask import json

import kadi.lib.constants as const
from kadi.modules.records.files import delete_file
from kadi.modules.records.models import File
from kadi.modules.records.models import FileState
from kadi.modules.records.models import Upload
from kadi.modules.records.uploads import delete_upload
from kadi.modules.records.utils import clean_files
from kadi.modules.records.utils import get_user_quota
from kadi.modules.records.utils import parse_import_data
from kadi.modules.templates.models import TemplateType
from tests.constants import DUMMY_EXTRA_VALUE


def test_get_user_quota(dummy_user, new_file, new_upload):
    """Test if calculating a user's quota works correctly."""
    assert get_user_quota(dummy_user) == 0

    # Add a file.
    file = new_file(file_data=10 * b"x")

    assert get_user_quota(dummy_user) == 10

    # Add an upload.
    new_upload(size=10)

    assert get_user_quota(dummy_user) == 20

    # Add a temporary upload that replaces the previous file with a smaller size.
    upload = new_upload(size=5, file=file)

    assert get_user_quota(dummy_user) == 20

    delete_upload(upload)

    # Add a temporary upload that replaces the previous file with a larger size.
    upload = new_upload(size=15, file=file)

    assert get_user_quota(dummy_user) == 25

    delete_upload(upload)

    # Add several uploads that replace the previous file with a larger size in total.
    new_upload(size=5, file=file)
    new_upload(size=5, file=file)
    new_upload(size=5, file=file)

    assert get_user_quota(dummy_user) == 25


def _encode_import_data(data):
    return BytesIO(json.dumps(data).encode())


def test_parse_import_data_json(new_record, new_template):
    """Test if JSON import data is parsed correctly."""

    # Test parsing record data.
    record_data = {"identifier": "test"}
    data = _encode_import_data(record_data)

    assert parse_import_data(data, const.IMPORT_TYPE_JSON) == record_data

    # Test parsing record template data.
    template_data = {
        "type": TemplateType.RECORD,
        "identifier": "test",
        "data": {"identifier": "test2"},
    }
    data = _encode_import_data(template_data)

    assert parse_import_data(data, const.IMPORT_TYPE_JSON) == template_data["data"]

    # Test parsing extras template data.
    template_data = {
        "type": TemplateType.EXTRAS,
        "identifier": "test",
        "data": [DUMMY_EXTRA_VALUE],
    }
    data = _encode_import_data(template_data)

    assert parse_import_data(data, const.IMPORT_TYPE_JSON) == {
        "extras": template_data["data"]
    }


def test_clean_files(
    monkeypatch, dummy_file, dummy_record, dummy_upload, dummy_user, new_upload
):
    """Test if cleaning files works correctly."""

    # To simulate an expired upload.
    new_upload()
    # To simulate an inactive upload.
    delete_upload(dummy_upload)
    # To simulate an inactive file.
    delete_file(dummy_file, user=dummy_user)

    clean_files()

    assert Upload.query.count() == 2
    assert File.query.one() == dummy_file
    assert dummy_file.state == FileState.INACTIVE

    monkeypatch.setattr("kadi.modules.records.utils.const.ACTIVE_UPLOADS_MAX_AGE", 0)
    monkeypatch.setattr("kadi.modules.records.utils.const.INACTIVE_UPLOADS_MAX_AGE", 0)
    monkeypatch.setattr("kadi.modules.records.utils.const.INACTIVE_FILES_MAX_AGE", 0)

    clean_files()

    assert not Upload.query.all()
    assert File.query.one() == dummy_file
    assert dummy_file.state == FileState.DELETED
