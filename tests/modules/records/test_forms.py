# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import pytest
from werkzeug.datastructures import MultiDict

import kadi.lib.constants as const
from kadi.lib.forms import KadiForm
from kadi.lib.permissions.core import add_role
from kadi.lib.resources.utils import add_link
from kadi.modules.records.forms import EditFileForm
from kadi.modules.records.forms import NewRecordForm
from kadi.modules.records.forms import RecordLinksField
from kadi.modules.records.forms import UploadChunkForm
from kadi.modules.records.links import create_record_link


@pytest.mark.parametrize(
    "data,is_valid,result",
    [
        (None, True, []),
        ({"test": "[]"}, True, []),
        (
            {"test": '[{"direction": "out", "record": 1, "name": "test"}]'},
            True,
            [{"direction": "out", "record": 1, "name": "test", "term": None}],
        ),
        # Invalid data, but valid data structure overall.
        ({"test": '[{"foo": "bar"}]'}, False, [{"foo": "bar"}]),
        # Invalid data structure.
        ({"test": '["test"]'}, False, []),
    ],
)
def test_record_links_field_submit(data, is_valid, result):
    """Test if submitting the custom "RecordLinksField" works correctly."""

    class _TestForm(KadiForm):
        test = RecordLinksField("Test")

    form = _TestForm(formdata=MultiDict(data))

    assert form.validate() is is_valid
    assert form.test.data == result

    if not is_valid:
        assert "Invalid data structure." in form.errors["test"]


def test_record_links_field_prefill(dummy_record, dummy_user, new_record, new_user):
    """Test if prefilling the custom "RecordLinksField" works correctly."""

    class _TestForm(KadiForm):
        test = RecordLinksField("Test")

    form = _TestForm(formdata=MultiDict())

    user = new_user()
    record_1 = new_record()
    record_2 = new_record(creator=user)

    add_role(user, "record", dummy_record.id, "collaborator")
    create_record_link(
        record_from=dummy_record, record_to=record_1, name="test1", creator=dummy_user
    )
    # Not readable by the dummy user.
    create_record_link(
        record_from=dummy_record, record_to=record_2, name="test2", creator=user
    )

    data = [
        {"direction": "out", "record": record_1.id, "name": "test1", "term": None},
        {"direction": "in", "record": record_2.id, "name": "test2", "term": None},
        {"record": "test", "foo": "bar"},
    ]
    form.test.set_initial_data(data=data, user=dummy_user)

    assert form.test.initial == [
        {
            "direction": "out",
            "record": [record_1.id, f"@{record_1.identifier}"],
            "name": "test1",
            "term": None,
        },
        {"direction": "in", "record": None, "name": "test2", "term": None},
        {"direction": "out", "record": None, "name": None, "term": None},
    ]

    form.test.set_initial_data(record=dummy_record, user=dummy_user)

    assert form.test.initial == [
        {
            "direction": "out",
            "record": [record_1.id, f"@{record_1.identifier}"],
            "name": "test1",
            "term": None,
        }
    ]


def test_new_record_form_import_data(dummy_license, dummy_user):
    """Test if prefilling a "NewRecordForm" with import data works correctly."""
    import_data = {
        "identifier": "test",
        "type": "test",
        "license": dummy_license.name,
        "tags": ["test"],
    }
    form = NewRecordForm(import_data=import_data, user=dummy_user)

    tag = import_data["tags"][0]
    record_type = import_data["type"]

    assert form.identifier.data == import_data["identifier"]
    assert form.type.initial == (record_type, record_type)
    assert form.license.initial == (dummy_license.name, dummy_license.title)
    assert form.tags.initial == [(tag, tag)]


def test_new_record_form_record(
    dummy_collection, dummy_license, dummy_user, new_collection, new_record, new_user
):
    """Test if prefilling a "NewRecordForm" with a record works correctly."""
    record = new_record(type="test", license=dummy_license.name, tags=["test"])

    user = new_user()
    # This collection should not appear in the linked collections in the form.
    collection = new_collection(creator=user)
    add_link(record.collections, collection, user=user)
    add_link(record.collections, dummy_collection, user=dummy_user)
    form = NewRecordForm(record=record, user=dummy_user)

    tag = record.tags.first().name

    assert form.identifier.data == record.identifier
    assert form.type.initial == (record.type, record.type)
    assert form.license.initial == (dummy_license.name, dummy_license.title)
    assert form.tags.initial == [(tag, tag)]
    assert form.collections.initial == [
        (dummy_collection.id, f"@{dummy_collection.identifier}")
    ]
    assert form.record_links.initial == []
    assert form.roles.initial == []


def test_new_record_form_template(
    dummy_collection, dummy_license, dummy_user, new_collection, new_template, new_user
):
    """Test if prefilling a "NewRecordForm" with a record template works correctly."""

    # This collection should not appear in the linked collections in the form.
    collection = new_collection(creator=new_user())

    template = new_template(
        data={
            "identifier": "test",
            "type": "test",
            "license": dummy_license.name,
            "tags": ["test"],
            "collections": [dummy_collection.id, collection.id],
            "record_links": [],
            "roles": [],
        }
    )

    form = NewRecordForm(template=template, user=dummy_user)

    template_type = template.data["type"]
    tag = template.data["tags"][0]

    assert form.identifier.data == template.data["identifier"]
    assert form.type.initial == (template_type, template_type)
    assert form.license.initial == (dummy_license.name, dummy_license.title)
    assert form.tags.initial == [(tag, tag)]
    assert form.collections.initial == [
        (dummy_collection.id, f"@{dummy_collection.identifier}")
    ]
    assert form.record_links.initial == []
    assert form.roles.initial == []


def test_new_record_form_collection(dummy_collection, dummy_user):
    """Test if prefilling a "NewRecordForm" with a collection works correctly."""
    form = NewRecordForm(collection=dummy_collection, user=dummy_user)
    assert form.collections.initial == [
        (dummy_collection.id, f"@{dummy_collection.identifier}")
    ]


@pytest.mark.parametrize(
    "chunk_count,index,size,is_valid",
    [
        (1, 0, 10, True),
        (1, 0, const.UPLOAD_CHUNK_SIZE, True),
        (1, 0, const.UPLOAD_CHUNK_SIZE + 1, False),
        (2, 0, 10, False),
    ],
)
def test_upload_chunk_form(chunk_count, index, size, is_valid):
    """Test if validating the "UploadChunkForm" works correctly."""
    form = UploadChunkForm(
        chunk_count, formdata=MultiDict({"index": str(index), "size": str(size)})
    )
    assert form.validate() is is_valid


def test_edit_file_form_validate_name(dummy_file, new_file):
    """Test if checking for duplicate files in the "EditFileForm" works correctly."""
    form = EditFileForm(dummy_file, formdata=MultiDict({"name": dummy_file.name}))

    assert form.validate()

    file = new_file()
    form = EditFileForm(dummy_file, formdata=MultiDict({"name": file.name}))

    assert not form.validate()
    assert "Name is already in use." in form.errors["name"]
