# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from io import BytesIO

import pytest
from flask import get_flashed_messages
from flask import json

import kadi.lib.constants as const
from kadi.lib.permissions.models import Role
from kadi.lib.web import url_for
from kadi.modules.templates.models import Template
from kadi.modules.templates.models import TemplateState
from kadi.modules.templates.models import TemplateType
from kadi.modules.templates.models import TemplateVisibility
from tests.utils import check_view_response


def test_templates(client, user_session):
    """Test the "templates.templates" endpoint."""
    with user_session():
        response = client.get(url_for("templates.templates"))
        check_view_response(response)


@pytest.mark.parametrize("template_type", [TemplateType.RECORD, TemplateType.EXTRAS])
def test_new_template_create(template_type, client, user_session):
    """Test the regular "templates.new_template" endpoint."""
    endpoint = url_for("templates.new_template", type=template_type)

    with user_session():
        response = client.get(endpoint)
        check_view_response(response)

        response = client.post(
            endpoint,
            data={
                "identifier": "test",
                "title": "test",
                "visibility": TemplateVisibility.PRIVATE,
            },
        )

        check_view_response(response, status_code=302)
        assert Template.query.filter_by(identifier="test").one()


@pytest.mark.parametrize("template_type", [TemplateType.RECORD, TemplateType.EXTRAS])
def test_new_template_import(template_type, client, user_session):
    """Test the file import of the "templates.new_template" endpoint."""
    endpoint = url_for("templates.new_template", type=template_type)
    import_data = {"identifier": "test"}

    with user_session():
        response = client.post(
            endpoint,
            data={
                "import_type": const.IMPORT_TYPE_JSON,
                "import_data": (BytesIO(json.dumps(import_data).encode()), "file"),
            },
            content_type=const.MIMETYPE_FORMDATA,
        )

        check_view_response(response)
        assert "File imported successfully." in get_flashed_messages()


@pytest.mark.parametrize("template_type", [TemplateType.RECORD, TemplateType.EXTRAS])
def test_edit_template(template_type, client, new_template, user_session):
    """Test the "templates.edit_template" endpoint."""
    template = new_template(type=template_type)
    endpoint = url_for("templates.edit_template", id=template.id)

    with user_session():
        response = client.get(endpoint)
        check_view_response(response)

        response = client.post(endpoint, data={"identifier": "test"})

        check_view_response(response, status_code=302)
        assert template.identifier == "test"


@pytest.mark.parametrize("template_type", [TemplateType.RECORD, TemplateType.EXTRAS])
def test_view_template(template_type, client, new_template, user_session):
    """Test the "templates.view_template" endpoint."""
    template = new_template(type=template_type)

    with user_session():
        response = client.get(url_for("templates.view_template", id=template.id))
        check_view_response(response)


@pytest.mark.parametrize("template_type", [TemplateType.RECORD, TemplateType.EXTRAS])
@pytest.mark.parametrize("export_type", const.EXPORT_TYPES["template"])
def test_export_template(
    template_type, export_type, client, new_template, user_session
):
    """Test the "templates.export_template" endpoint."""
    template = new_template(type=template_type)

    with user_session():
        response = client.get(
            url_for(
                "templates.export_template", id=template.id, export_type=export_type
            )
        )
        check_view_response(response)


def test_manage_permissions(
    client, dummy_group, dummy_template, new_user, user_session
):
    """Test the "templates.manage_permissions" endpoint."""
    endpoint = url_for("templates.manage_permissions", id=dummy_template.id)
    new_role = "member"
    user = new_user()

    with user_session():
        response = client.get(endpoint)
        check_view_response(response)

        response = client.post(
            endpoint,
            data={
                "roles": json.dumps(
                    [
                        {
                            "subject_type": "user",
                            "subject_id": user.id,
                            "role": new_role,
                        },
                        {
                            "subject_type": "group",
                            "subject_id": dummy_group.id,
                            "role": new_role,
                        },
                    ]
                )
            },
        )

        check_view_response(response, status_code=302)
        assert user.roles.filter(
            Role.object == "template",
            Role.object_id == dummy_template.id,
            Role.name == new_role,
        ).one()
        assert dummy_group.roles.filter(
            Role.object == "template",
            Role.object_id == dummy_template.id,
            Role.name == new_role,
        ).one()


def test_view_revision(client, dummy_template, user_session):
    """Test the "templates.view_revision" endpoint."""
    with user_session():
        response = client.get(
            url_for(
                "templates.view_revision",
                template_id=dummy_template.id,
                revision_id=dummy_template.ordered_revisions.first().id,
            )
        )
        check_view_response(response)


def test_delete_template(client, dummy_template, user_session):
    """Test the "templates.delete_template" endpoint."""
    with user_session():
        response = client.post(
            url_for("templates.delete_template", id=dummy_template.id)
        )

        check_view_response(response, status_code=302)
        assert dummy_template.state == TemplateState.DELETED
