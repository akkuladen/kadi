# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import pytest
from marshmallow import ValidationError

from kadi.modules.templates.models import TemplateType
from kadi.modules.templates.schemas import TemplateSchema
from tests.constants import DEFAULT_RECORD_TEMPLATE_DATA
from tests.constants import DUMMY_EXTRA_VALUE


@pytest.mark.parametrize(
    "data,is_valid,result",
    [
        ({}, True, None),
        ({"title": " ", "identifier": " "}, True, {"title": "", "identifier": ""}),
        ({"tags": [" B ", "a", "c   d", "A"]}, True, {"tags": ["a", "b", "c d"]}),
        (
            {
                "title": "test",
                "identifier": "test",
                "type": "test",
                "description": "test",
                # Use the name of the dummy license.
                "license": "test",
                "tags": ["test"],
                "extras": [DUMMY_EXTRA_VALUE],
                "collections": [123],
                "record_links": [
                    {"direction": "out", "record": 1, "name": "test", "term": None},
                ],
                "roles": [
                    {"subject_type": "user", "subject_id": 123, "role": "member"}
                ],
            },
            True,
            None,
        ),
        ({"record_links": [{}]}, False, None),
        ({"roles": [{}]}, False, None),
    ],
)
def test_template_schema_record_data(data, is_valid, result, dummy_license):
    """Test if loading record data in the "TemplateSchema" works correctly."""
    schema = TemplateSchema(only=["data"], template_type=TemplateType.RECORD)

    if result is None:
        result = data

    if is_valid:
        data = schema.load({"data": data})
        assert data["data"] == {**DEFAULT_RECORD_TEMPLATE_DATA, **result}
    else:
        with pytest.raises(ValidationError):
            schema.load({"data": data})


@pytest.mark.parametrize("data", [[], [DUMMY_EXTRA_VALUE]])
def test_template_schema_extras_data(data):
    """Test if loading extras data in the "TemplateSchema" works correctly."""
    schema = TemplateSchema(only=["data"], template_type=TemplateType.EXTRAS)
    assert schema.load({"data": data})["data"] == data
