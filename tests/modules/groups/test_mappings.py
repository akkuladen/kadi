# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from kadi.modules.groups.mappings import GroupMapping


def test_group_mapping(dummy_group):
    """Test if the mapping for groups works correctly."""
    doc = GroupMapping.create_document(dummy_group)

    assert doc.meta.id == dummy_group.id
    assert doc.identifier == dummy_group.identifier
