# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from kadi.lib.permissions.core import add_role
from kadi.modules.groups.core import create_group
from kadi.modules.groups.core import delete_group
from kadi.modules.groups.core import purge_group
from kadi.modules.groups.core import restore_group
from kadi.modules.groups.core import update_group
from kadi.modules.groups.models import Group
from kadi.modules.groups.models import GroupState
from kadi.modules.groups.models import GroupVisibility


def test_create_group(dummy_user):
    """Test if groups are created correctly."""
    group = create_group(
        creator=dummy_user,
        identifier="test",
        title="test",
        description="# test",
        visibility=GroupVisibility.PRIVATE,
    )

    assert Group.query.filter_by(identifier="test").one() == group
    assert group.plain_description == "test"
    assert group.revisions.count() == 1
    assert dummy_user.roles.filter_by(
        name="admin", object="group", object_id=group.id
    ).one()


def test_update_group(dummy_group, dummy_user):
    """Test if groups are updated correctly."""
    update_group(dummy_group, description="# test", user=dummy_user)

    assert dummy_group.plain_description == "test"
    assert dummy_group.revisions.count() == 2


def test_delete_group(dummy_group, dummy_user):
    """Test if groups are deleted correctly."""
    delete_group(dummy_group, user=dummy_user)

    assert dummy_group.state == GroupState.DELETED
    assert dummy_group.revisions.count() == 2


def test_restore_group(dummy_group, dummy_user):
    """Test if groups are restored correctly."""
    delete_group(dummy_group, user=dummy_user)
    restore_group(dummy_group, user=dummy_user)

    assert dummy_group.state == GroupState.ACTIVE
    assert dummy_group.revisions.count() == 3


def test_purge_group(db, new_group, new_user):
    """Test if groups are purged correctly."""
    user = new_user()
    group = new_group()

    add_role(user, "group", group.id, "member")
    db.session.commit()

    purge_group(group)

    assert Group.query.get(group.id) is None
    # Only the system role should remain.
    assert user.roles.count() == 1
