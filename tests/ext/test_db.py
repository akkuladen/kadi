# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import pytest
from werkzeug.exceptions import NotFound

from kadi.modules.records.core import delete_record
from kadi.modules.records.models import Record


def test_kadi_model(dummy_collection, dummy_record, new_record):
    """Test if the custom model class works correctly."""
    # pylint: disable=comparison-with-itself
    assert dummy_record == dummy_record
    assert dummy_record == Record.query.get(dummy_record.id)
    assert dummy_record != new_record()
    assert dummy_record != object()

    # Make sure both dummy resources have the same ID first (even if they are not
    # persisted).
    dummy_record.id = dummy_collection.id = 1
    assert dummy_record != dummy_collection


def test_kadi_base_query(dummy_record, dummy_user):
    """Test if the custom base query class works correctly."""
    assert Record.query.get_active(dummy_record.id) == dummy_record
    assert Record.query.get_active_or_404(dummy_record.id) == dummy_record

    delete_record(dummy_record, user=dummy_user)

    assert not Record.query.get_active(dummy_record.id)

    with pytest.raises(NotFound):
        Record.query.get_active_or_404(dummy_record.id)
