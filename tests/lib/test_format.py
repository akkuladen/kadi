# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import pytest

import kadi.lib.constants as const
from kadi.lib.conversion import parse_datetime_string
from kadi.lib.format import duration
from kadi.lib.format import filesize
from kadi.lib.format import pretty_type_name
from kadi.lib.format import timestamp
from kadi.modules.records.models import Record


@pytest.mark.parametrize(
    "value,result",
    [
        (0, "0 seconds"),
        (1, "1 second"),
        (const.ONE_MINUTE, "1 minute"),
        (const.ONE_HOUR, "1 hour"),
        (const.ONE_DAY, "1 day"),
        (const.ONE_WEEK, "1 week"),
        (
            const.ONE_WEEK + const.ONE_DAY + const.ONE_HOUR + const.ONE_MINUTE + 1,
            "1 week, 1 day, 1 hour, 1 minute, 1 second",
        ),
    ],
)
def test_duration(value, result):
    """Test if durations are formatted correctly."""
    assert duration(value) == result


@pytest.mark.parametrize(
    "value,result",
    [
        (0, "0 Bytes"),
        (1, "1 Byte"),
        (1.5, "1 Byte"),
        ("2.5", "2 Bytes"),
        (const.ONE_KB, "1 kB"),
        (1.5 * const.ONE_KB, "1.5 kB"),
        (1.54 * const.ONE_KB, "1.5 kB"),
        (1.55 * const.ONE_KB, "1.6 kB"),
        (1.56 * const.ONE_KB, "1.6 kB"),
        (10 * const.ONE_MB, "10 MB"),
        (100 * const.ONE_GB, "100 GB"),
        (const.ONE_TB, "1 TB"),
        (1500 * 1000 * const.ONE_TB, "1,500 PB"),
    ],
)
def test_filesize(value, result):
    """Test if file sizes are formatted correctly."""
    assert filesize(value) == result


def test_timestamp():
    """Test if timestamps are generated correctly."""
    date_time = parse_datetime_string("2020-01-01T12:34:56.789Z")

    assert timestamp(date_time=date_time) == "20200101123456"
    assert timestamp(date_time=date_time, include_micro=True) == "20200101123456789000"


@pytest.mark.parametrize(
    "value,result",
    [
        ("str", "string"),
        (str, "string"),
        ("record", "record"),
        (Record, "Record"),
    ],
)
def test_pretty_type_name(value, result):
    """Test if type names are prettified correctly."""
    assert pretty_type_name(value) == result
