# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import pytest

import kadi.lib.constants as const
from kadi.lib.permissions.core import add_role
from kadi.lib.permissions.core import apply_role_rule
from kadi.lib.permissions.core import create_role_rule
from kadi.lib.permissions.core import get_permitted_objects
from kadi.lib.permissions.core import has_permission
from kadi.lib.permissions.core import remove_role
from kadi.lib.permissions.core import remove_role_rule
from kadi.lib.permissions.core import set_system_role
from kadi.lib.permissions.models import Role
from kadi.lib.permissions.models import RoleRule
from kadi.lib.permissions.models import RoleRuleType
from kadi.modules.records.models import RecordVisibility


def test_has_permission(dummy_group, dummy_record, dummy_user, new_record, new_user):
    """Test if permissions are determined correctly."""
    action = "read"
    object_name = "record"
    role = "member"

    # Test with invalid parameters.
    assert not has_permission(dummy_user, "invalid", object_name, dummy_record.id)
    assert not has_permission(dummy_user, action, "invalid", dummy_record.id)
    assert not has_permission(dummy_user, action, object_name, dummy_record.id + 1)

    # Test global actions with a corresponding system role.
    user = new_user()
    set_system_role(user, "admin")

    assert not has_permission(dummy_user, action, object_name, None)
    assert has_permission(user, action, object_name, None)
    assert has_permission(user, action, object_name, dummy_record.id)

    # Test default permissions via the visibility value.
    record = new_record(creator=new_user(), visibility=RecordVisibility.PUBLIC)

    assert has_permission(dummy_user, action, object_name, record.id)
    assert not has_permission(
        dummy_user, action, object_name, record.id, check_defaults=False
    )

    # Test invalid permissions.
    user = new_user()
    record = new_record(creator=user)

    assert not has_permission(dummy_user, action, object_name, record.id)
    assert not has_permission(dummy_group, action, object_name, dummy_record.id)
    assert not has_permission(user, action, object_name, dummy_record.id)

    # Test valid permissions with and without group permission checks.
    user_1 = new_user()
    user_2 = new_user()

    add_role(user_1, "group", dummy_group.id, role)
    add_role(user_2, object_name, dummy_record.id, role)
    add_role(dummy_group, object_name, dummy_record.id, role)

    assert has_permission(dummy_user, action, object_name, dummy_record.id)
    assert has_permission(dummy_group, action, object_name, dummy_record.id)
    assert has_permission(user_1, action, object_name, dummy_record.id)
    assert has_permission(user_2, action, object_name, dummy_record.id)

    assert has_permission(
        dummy_user, action, object_name, dummy_record.id, check_groups=False
    )
    assert has_permission(
        dummy_group, action, object_name, dummy_record.id, check_groups=False
    )
    assert not has_permission(
        user_1, action, object_name, dummy_record.id, check_groups=False
    )
    assert has_permission(
        user_2, action, object_name, dummy_record.id, check_groups=False
    )


def test_get_permitted_objects(
    dummy_group, dummy_record, dummy_user, new_group, new_record, new_user
):
    """Test if permitted objects are determined correctly."""
    action = "read"
    object_name = "record"
    role = "member"

    # Test with invalid parameters.
    assert not get_permitted_objects(dummy_user, "invalid", object_name).all()
    assert get_permitted_objects(dummy_user, action, "invalid") is None

    # Test global actions with a corresponding system role.
    user = new_user()
    set_system_role(user, "admin")

    assert get_permitted_objects(user, action, object_name).one() == dummy_record

    # Test default permissions via the visibility value.
    record = new_record(creator=new_user(), visibility=RecordVisibility.PUBLIC)

    assert get_permitted_objects(dummy_user, action, object_name).count() == 2
    assert (
        get_permitted_objects(
            dummy_user, action, object_name, check_defaults=False
        ).one()
        == dummy_record
    )

    # Prevent the record from being accessible for the following assertions.
    record.visibility = RecordVisibility.PRIVATE

    # Test valid permissions with and without group permission checks.
    user_1 = new_user()
    user_2 = new_user()

    add_role(user_1, "group", dummy_group.id, role)
    add_role(user_2, object_name, dummy_record.id, role)
    add_role(dummy_group, object_name, dummy_record.id, role)

    assert get_permitted_objects(dummy_user, action, object_name).one() == dummy_record
    assert get_permitted_objects(dummy_group, action, object_name).one() == dummy_record
    assert get_permitted_objects(user_1, action, object_name).one() == dummy_record
    assert get_permitted_objects(user_2, action, object_name).one() == dummy_record

    assert (
        get_permitted_objects(dummy_user, action, object_name, check_groups=False).one()
        == dummy_record
    )
    assert (
        get_permitted_objects(
            dummy_group, action, object_name, check_groups=False
        ).one()
        == dummy_record
    )
    assert not get_permitted_objects(
        user_1, action, object_name, check_groups=False
    ).all()
    assert (
        get_permitted_objects(user_2, action, object_name, check_groups=False).one()
        == dummy_record
    )


def test_add_role(dummy_group, dummy_record, new_user):
    """Test if adding roles works correctly."""
    user = new_user()
    role = "member"
    prev_timestamp = dummy_record.last_modified

    with pytest.raises(ValueError) as e:
        add_role(user, "test", dummy_record.id, role)
        assert str(e) == "Object type 'test' does not exist."

    with pytest.raises(ValueError) as e:
        add_role(user, "record", dummy_record.id + 1, role)
        assert (
            str(e) == f"Object 'record' with ID {dummy_record.id + 1} does not exist."
        )

    with pytest.raises(ValueError) as e:
        # Test adding the role to itself, even though group roles are not currently
        # implemented for groups.
        add_role(dummy_group, "group", dummy_group.id, role)
        assert str(e) == "Cannot add a role to the object to which the role refers."

    with pytest.raises(ValueError) as e:
        add_role(user, "record", dummy_record.id, "invalid")
        assert str(e) == "A role with that name does not exist."

    assert add_role(user, "record", dummy_record.id, role)
    assert not add_role(user, "record", dummy_record.id, role)
    assert user.roles.filter(
        Role.object == "record", Role.object_id == dummy_record.id, Role.name == role
    ).one()
    assert dummy_record.last_modified != prev_timestamp


def test_remove_role(db, dummy_record, new_user):
    """Test if removing roles works correctly."""
    user = new_user()
    role = "member"

    with pytest.raises(ValueError) as e:
        remove_role(user, "test", dummy_record.id, role)
        assert str(e) == "Object type 'test' does not exist."

    with pytest.raises(ValueError) as e:
        remove_role(user, "record", dummy_record.id + 1, role)
        assert (
            str(e) == f"Object 'record' with ID {dummy_record.id + 1} does not exist."
        )

    assert not remove_role(user, "record", dummy_record.id, role)

    add_role(user, "record", dummy_record.id, role)
    db.session.commit()
    prev_timestamp = dummy_record.last_modified

    assert remove_role(user, "record", dummy_record.id, role)

    db.session.commit()

    assert (
        user.roles.filter(
            Role.object == "record", Role.object_id == dummy_record.id
        ).first()
        is None
    )
    assert dummy_record.last_modified != prev_timestamp


def test_set_system_role(dummy_user):
    """Test if system roles are set correctly."""
    role_query = dummy_user.roles.filter(
        Role.object.is_(None), Role.object_id.is_(None)
    )

    assert not set_system_role(dummy_user, "test")
    assert role_query.one().name == "member"

    assert set_system_role(dummy_user, "admin")
    assert role_query.one().name == "admin"


def test_create_role_rule(db, dummy_record):
    """Test if role rules are created correctly."""
    role_name = "member"
    prev_timestamp = dummy_record.last_modified

    # Try to create an invalid role rule.
    rule_type = RoleRuleType.USERNAME

    assert create_role_rule("test", dummy_record.id, "member", rule_type, {}) is None
    assert (
        create_role_rule("record", dummy_record.id + 1, "member", rule_type, {}) is None
    )
    assert create_role_rule("record", dummy_record.id, "test", rule_type, {}) is None
    assert (
        create_role_rule("record", dummy_record.id, "member", rule_type, "test") is None
    )

    # Try to create a valid role rule.
    role_rule = create_role_rule("record", dummy_record.id, role_name, "test", {})
    db.session.commit()

    assert RoleRule.query.filter_by(type="test").one() == role_rule
    assert role_rule.role.object == "record"
    assert role_rule.role.object_id == dummy_record.id
    assert role_rule.role.name == role_name
    assert dummy_record.last_modified != prev_timestamp


def test_remove_role_rule(db, dummy_record):
    """Test if role rules are removed correctly."""
    role_rule = create_role_rule("record", dummy_record.id, "member", "test", {})
    db.session.commit()

    prev_timestamp = dummy_record.last_modified
    remove_role_rule(role_rule)
    db.session.commit()

    assert not RoleRule.query.all()
    assert dummy_record.last_modified != prev_timestamp


def test_apply_role_rule(db, dummy_record, new_user):
    """Test if role rules are applied correctly."""
    role_name = "member"
    role_filter = [
        Role.object == "record",
        Role.object_id == dummy_record.id,
        Role.name == role_name,
    ]
    user_1 = new_user(username="test_1_123_")
    user_2 = new_user(username="test_2_123_")
    user_3 = new_user(username="test_3_1234")

    role_rule = create_role_rule(
        "record",
        dummy_record.id,
        role_name,
        RoleRuleType.USERNAME,
        {"identity_type": const.AUTH_PROVIDER_TYPE_LOCAL, "pattern": "test*123_"},
    )
    db.session.commit()

    # Only "user_1" should receive the role.
    apply_role_rule(role_rule, user=user_1)

    assert user_1.roles.filter(*role_filter).one() is not None
    assert user_2.roles.filter(*role_filter).first() is None
    assert user_3.roles.filter(*role_filter).first() is None

    # Now, "user_2" should receive the role as well.
    apply_role_rule(role_rule)

    assert user_1.roles.filter(*role_filter).one() is not None
    assert user_2.roles.filter(*role_filter).one() is not None
    assert user_3.roles.filter(*role_filter).first() is None
