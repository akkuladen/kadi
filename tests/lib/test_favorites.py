# Copyright 2022 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
from kadi.lib.favorites.core import delete_favorites
from kadi.lib.favorites.core import is_favorite
from kadi.lib.favorites.core import toggle_favorite
from kadi.lib.favorites.models import Favorite


def test_is_favorite(dummy_record, dummy_user):
    """Test if favorites are determined correctly."""
    object_name = "record"

    assert not is_favorite(object_name, dummy_record.id, user=dummy_user)
    assert not dummy_record.is_favorite(user=dummy_user)

    Favorite.create(user=dummy_user, object=object_name, object_id=dummy_record.id)

    assert is_favorite(object_name, dummy_record.id, user=dummy_user)
    assert dummy_record.is_favorite(user=dummy_user)


def test_toggle_favorite(db, dummy_record, dummy_user):
    """Test if favorites are toggled correctly."""
    object_name = "record"

    assert not dummy_user.favorites.all()

    toggle_favorite(object_name, dummy_record.id, user=dummy_user)
    assert dummy_user.favorites.one()

    toggle_favorite(object_name, dummy_record.id, user=dummy_user)
    assert not dummy_user.favorites.all()


def test_delete_favorites(db, dummy_record, new_user):
    """Test if favorites are deleted correctly."""
    object_name = "record"

    toggle_favorite(object_name, dummy_record.id, user=new_user())
    toggle_favorite(object_name, dummy_record.id, user=new_user())

    delete_favorites(object_name, dummy_record.id)
    assert not Favorite.query.all()
