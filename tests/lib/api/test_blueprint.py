# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import pytest
from werkzeug.routing import BuildError

import kadi.lib.constants as const
from kadi.lib.web import url_for
from tests.utils import check_api_response


def test_api_versioning(api_client, client, dummy_personal_token, user_session):
    """Test if API versioning works correctly."""

    # Test a versioned endpoint.
    endpoint = url_for("api.index")

    response = api_client(dummy_personal_token).get(url_for("api.index"))
    check_api_response(response)

    for version in const.API_VERSIONS:
        endpoint = url_for(f"api.index_{version}")

        response = api_client(dummy_personal_token).get(endpoint)
        check_api_response(response)

    # Test an unversioned endpoint.
    endpoint = url_for("api.quick_search")

    # We need to use the session as the endpoint is internal.
    with user_session():
        response = client.get(endpoint)
        check_api_response(response)

        for version in const.API_VERSIONS:
            with pytest.raises(BuildError):
                url_for(f"api.quick_search_{version}")
