# Copyright 2020 Karlsruhe Institute of Technology
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
import pytest

from kadi.lib.exceptions import KadiPermissionError
from kadi.lib.permissions.core import add_role
from kadi.lib.resources.utils import add_link
from kadi.lib.resources.utils import clean_resources
from kadi.lib.resources.utils import get_filtered_resources
from kadi.lib.resources.utils import get_linked_resources
from kadi.lib.resources.utils import remove_link
from kadi.modules.collections.core import delete_collection
from kadi.modules.collections.models import Collection
from kadi.modules.groups.core import delete_group
from kadi.modules.groups.models import Group
from kadi.modules.records.core import delete_record
from kadi.modules.records.models import Record
from kadi.modules.templates.core import delete_template
from kadi.modules.templates.models import Template


def test_get_filtered_resources(dummy_record, dummy_user):
    """Test if resources are filtered correctly."""
    assert get_filtered_resources(Record, user=dummy_user).one().id == dummy_record.id
    assert (
        get_filtered_resources(Record, user_ids=[dummy_user.id], user=dummy_user)
        .one()
        .id
        == dummy_record.id
    )
    assert not get_filtered_resources(
        Record, visibility="public", user=dummy_user
    ).all()


def test_add_link(dummy_collection, dummy_record, dummy_user, new_user):
    """Test if linking two resources works correctly."""
    user = new_user()

    with pytest.raises(KadiPermissionError):
        add_link(dummy_record.collections, dummy_collection, user=user)

    assert add_link(dummy_record.collections, dummy_collection, user=dummy_user)
    assert dummy_record.collections.one() == dummy_collection
    # This is essentially the same link as before, so it is expected to fail.
    assert not add_link(dummy_collection.records, dummy_record, user=dummy_user)


def test_remove_link(db, dummy_collection, dummy_record, dummy_user, new_user):
    """Test if unlinking two resources works correctly."""
    user = new_user()
    add_link(dummy_record.collections, dummy_collection, user=dummy_user)
    db.session.commit()

    with pytest.raises(KadiPermissionError):
        remove_link(dummy_record.collections, dummy_collection, user=user)

    assert remove_link(dummy_record.collections, dummy_collection, user=dummy_user)

    db.session.commit()

    assert not dummy_record.collections.all()
    # The second call is expected to fail if the link was removed successfully.
    assert not remove_link(dummy_record.collections, dummy_collection, user=dummy_user)


def test_get_linked_resources(dummy_collection, dummy_record, dummy_user, new_user):
    """Test if determining linked resources works correctly."""
    user = new_user()
    add_link(dummy_record.collections, dummy_collection, user=dummy_user)

    # Check the linked resources of the resource creator.
    linked_resources = get_linked_resources(
        Collection, dummy_record.collections, user=dummy_user
    )
    assert linked_resources.one() == dummy_collection

    linked_resources = get_linked_resources(
        Collection, dummy_record.collections, user=dummy_user, actions=["link"]
    )
    assert linked_resources.one() == dummy_collection

    # Check the linked resources if the given actions are invalid.
    linked_resources = get_linked_resources(
        Collection, dummy_record.collections, user=dummy_user, actions=["invalid"]
    )
    assert not linked_resources.all()

    # Check the linked resources of a user who is not permitted.
    linked_resources = get_linked_resources(
        Collection, dummy_record.collections, user=user
    )
    assert not linked_resources.all()

    # Check the linked resources of a user who is only permitted to read them.
    add_role(user, "collection", dummy_collection.id, "member")

    linked_resources = get_linked_resources(
        Collection, dummy_record.collections, user=user
    )
    assert linked_resources.one() == dummy_collection

    linked_resources = get_linked_resources(
        Collection, dummy_record.collections, user=user, actions=["link"]
    )
    assert not linked_resources.all()


def test_clean_resources(
    monkeypatch, dummy_collection, dummy_group, dummy_record, dummy_template, dummy_user
):
    """Test if cleaning resources works correctly."""
    delete_collection(dummy_collection, user=dummy_user)
    delete_group(dummy_group, user=dummy_user)
    delete_record(dummy_record, user=dummy_user)
    delete_template(dummy_template, user=dummy_user)

    clean_resources()

    assert Collection.query.one()
    assert Group.query.one()
    assert Record.query.one()
    assert Template.query.one()

    monkeypatch.setattr("kadi.lib.resources.utils.const.DELETED_RESOURCES_MAX_AGE", 0)
    clean_resources()

    assert not Collection.query.all()
    assert not Group.query.all()
    assert not Record.query.all()
    assert not Template.query.all()
