#!/usr/bin/env bash
set -e

cd "$(dirname $(readlink -f $0))/.."

# Install Node.js.
apt update
apt install -y ca-certificates curl gnupg
install -m 0755 -d /etc/apt/keyrings
curl -fsSL https://deb.nodesource.com/gpgkey/nodesource-repo.gpg.key | gpg --dearmor -o /etc/apt/keyrings/nodesource.gpg
echo "deb [signed-by=/etc/apt/keyrings/nodesource.gpg] https://deb.nodesource.com/node_20.x nodistro main" > /etc/apt/sources.list.d/nodesource.list
apt update && apt install -y nodejs

# Install the application with all development requirements.
pip3 install -e .[dev]

# Use the development environment when using the Kadi CLI.
export KADI_ENV="development"

# Compile the assets and translations.
kadi assets build
kadi i18n compile

# Build the wheel.
python3 -m build --wheel

# Upload the wheel to PyPI.
twine upload dist/*
